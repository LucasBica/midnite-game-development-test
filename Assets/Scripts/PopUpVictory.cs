﻿using MessageSystem;

using UnityEngine;
using UnityEngine.UI;

using Utilities;

namespace Namespace {

    public class PopUpVictory : PopUp {

        [Header("References")]
        [SerializeField] private Button buttonReset = default;

        private void Awake() {
            MessageManager.AttachListener(MessageTypes.ShowPopUpVictory, ShowPopUpVictory);
            buttonReset.onClick.AddListener(OnClick);
        }

        private void OnDestroy() {
            MessageManager.DetachListener(MessageTypes.ShowPopUpVictory, ShowPopUpVictory);
        }

        private void ShowPopUpVictory(MessageClass message) {
            Show();
        }

        private void OnClick() {
            Singleton.Instance.SceneController.LoadScene(SceneController.ScenesTypes.SampleScene);
        }
    }
}