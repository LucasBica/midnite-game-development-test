﻿using System.Collections.Generic;

using UnityEngine;

namespace Namespace {

    public class ObjectGrid : UnityObject {

        [Header("Settings")]
        [SerializeField] private float animDuration = default;

        [Header("Assets")]
        [SerializeField] private PiecesReferences piecesReferences = default;
        [SerializeField] private AnimationCurve curveY = default;
        [SerializeField] private AnimationCurve curveXZ = default;
        [SerializeField] private AnimationCurve curveRotation = default;

        private Vector2 direction;
        private Vector3 startPosition;
        private Vector3 targetPosition;
        private float delta;
        private bool playAnimation;

        public Vector2Int Coor { get; set; }

        public int Value { get; private set; }

        private void Update() {
            if (!playAnimation) {
                return;
            }

            delta = animDuration == 0 ? 1 : delta + Time.deltaTime / animDuration; // We cannot divide by zero.
            if (delta >= 1) {
                delta = 1;
                playAnimation = false;
            }

            bool xIsZero = direction.x == 0;

            Vector3 position = new Vector3(
                xIsZero ? startPosition.x : Mathf.LerpUnclamped(startPosition.x, targetPosition.x, curveXZ.Evaluate(delta)),
                Mathf.LerpUnclamped(startPosition.y, targetPosition.y, curveY.Evaluate(delta)),
                !xIsZero ? startPosition.z : Mathf.LerpUnclamped(startPosition.z, targetPosition.z, curveXZ.Evaluate(delta)));

            float valueCurve = curveRotation.Evaluate(delta);

            Vector3 eulerAngles = new Vector3(
                !xIsZero ? 0f : Mathf.LerpUnclamped(0, 180 * direction.y, valueCurve),
                0f,
                xIsZero ? 0f : Mathf.LerpUnclamped(0, 180 * -direction.x, valueCurve));

            transform.localPosition = position;
            transform.localEulerAngles = eulerAngles;
        }

        public void Initialize(Vector2Int coor, int value) {
            Coor = coor;
            Value = value;

            if (value == 0) {
                Instantiate(piecesReferences.GetBread(), transform, false);
            } else {
                Instantiate(piecesReferences.GetRandom(), transform, false);
            }
        }

        public void Move(Vector2Int direction, List<ObjectGrid> cellToMove, List<ObjectGrid> cellDestination) {
            Coor += direction;

            transform.SetParent(cellDestination[0].transform);

            this.direction = direction;
            startPosition = transform.localPosition;
            targetPosition = new Vector3(startPosition.x + direction.x, (cellToMove.Count + cellDestination.Count) * 0.1f, startPosition.z + direction.y);
            delta = 0;
            playAnimation = true;
        }
    }
}