﻿using UnityEngine;
using UnityEngine.UI;

using Utilities;

namespace Namespace {

    public class CanvasGamePlay : UnityObject {

        [Header("References")]
        [SerializeField] private Button buttonReset = default;
        [SerializeField] private RectTransform rectButton = default;

        private void Awake() {
            buttonReset.onClick.AddListener(OnClick);
        }

        private void Start() {
            rectButton.anchoredPosition = new Vector2(rectButton.anchoredPosition.x, rectButton.anchoredPosition.y - NotchResponsive.Height());
        }

        private void OnClick() {
            Singleton.Instance.SceneController.LoadScene(SceneController.ScenesTypes.SampleScene);
        }
    }
}