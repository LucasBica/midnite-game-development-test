﻿using UnityEngine;

namespace Utilities {

    public class NotchResponsive : UnityObject {

        public static bool IsNotch() => Screen.cutouts.Length > 0;

        public static float Height() => IsNotch() ? Screen.cutouts[0].height : 0f;
    }
}