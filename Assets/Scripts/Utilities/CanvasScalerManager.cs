﻿using UnityEngine;
using UnityEngine.UI;

namespace Utilities {

    public class CanvasScalerManager : UIObject {

        [Header("Settings")]
        [SerializeField] private bool invertValue = default;

        [Header("References")]
        [SerializeField] private CanvasScaler canvasScaler = default;

        private void Awake() {
            CalculateMatch();
        }

#if UNITY_EDITOR
        private void Update() {
            CalculateMatch();
        }
#endif

        private void CalculateMatch() {
            float aspect = (float)Screen.height / Screen.width;
            canvasScaler.matchWidthOrHeight = aspect > .5625f ? invertValue ? 1 : 0 : invertValue ? 0 : 1;
        }
    }
}