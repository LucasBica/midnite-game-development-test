﻿using System;

using UnityEngine;

namespace Utilities {

    public class Singleton : UnityObject {

        [Header("References")]
        [SerializeField] private SceneController sceneController = default;

        public static Singleton Instance { get; private set; }

        public SceneController SceneController => sceneController;

        public Action onUpdate;

        private void Awake() {
            if (Instance == null) {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            } else {
                Destroy(gameObject);
            }
        }

        private void OnDestroy() {
            if (Instance == this) {
                Instance = null;
            }
        }

        private void Update() {
            onUpdate?.Invoke();
        }
    }
}