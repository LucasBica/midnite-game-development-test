﻿using UnityEngine.SceneManagement;

namespace Utilities {

    public class SceneController : UnityObject {

        public void LoadScene(ScenesTypes sceneType) {
            SceneManager.LoadScene(sceneType.ToString());
        }

        public enum ScenesTypes {
            SampleScene,
        }
    }
}