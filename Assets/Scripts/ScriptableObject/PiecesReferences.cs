﻿using UnityEngine;

namespace Namespace {

    [CreateAssetMenu(fileName = nameof(PiecesReferences), menuName = "Midnite/ScriptableObjects/" + nameof(PiecesReferences), order = 1)]
    public class PiecesReferences : ScriptableObject {

        [Header("References")]
        [SerializeField] private GameObject prefabBread = default;
        [SerializeField] private GameObject[] prefabsIngredients = default;

        public GameObject GetBread() => prefabBread;

        public GameObject GetRandom() => prefabsIngredients[Random.Range(0, prefabsIngredients.Length - 1)];
    }
}