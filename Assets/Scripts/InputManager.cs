﻿using System;

using UnityEngine;

using Utilities;

namespace Namespace {

    public class InputManager : UnityObject {

        [Header("Settings")]
        [Range(0f, 0.1f)][SerializeField] private float sensibility = default; // After swiping your finger on 1/32 of your screen, it is considered a swipe action: 1 / 32 = 0.03125f

        private float pixelsRadius;
        private Vector2 startPixelPosition = new Vector2(-1, -1);

        private readonly Vector2 invalidPosition = new Vector2(-1, -1);

        public event Action<Vector2, Vector2Int> OnSwipe;

        private void Start() {
            int pixels = Math.Max(Screen.width, Screen.height);
            pixelsRadius = sensibility == 0 ? pixels : pixels * sensibility;
        }

        private void Update() {
            if (Input.GetKeyUp(KeyCode.R)) {
                Singleton.Instance.SceneController.LoadScene(SceneController.ScenesTypes.SampleScene);
                return;
            }

            if (Input.GetMouseButtonDown(0)) {
                startPixelPosition = Input.mousePosition;
            }

            if (startPixelPosition != invalidPosition) {

                Vector2 inputPosition = Vector2.zero;
                bool posSetted = false;

                if (Input.GetMouseButton(0)) {
                    inputPosition = Input.mousePosition;
                    posSetted = true;

                } else if (Input.touchCount > 0) {

                    Touch touch = Input.GetTouch(0);

                    if (touch.phase == TouchPhase.Moved) {
                        inputPosition = touch.position;
                        posSetted = true;
                    }
                }

                if (posSetted) {
                    float x = inputPosition.x - startPixelPosition.x;
                    float y = inputPosition.y - startPixelPosition.y;

                    float xAbs = Mathf.Abs(x);
                    float yAbs = Mathf.Abs(y);

                    if (xAbs > pixelsRadius || yAbs > pixelsRadius) {

                        if (xAbs >= yAbs) {
                            OnSwipe?.Invoke(startPixelPosition, new Vector2Int(x > 0 ? 1 : -1, 0));
                        } else {
                            OnSwipe?.Invoke(startPixelPosition, new Vector2Int(0, y > 0 ? 1 : -1));
                        }

                        startPixelPosition = invalidPosition;
                    }
                }
            }
        }
    }
}