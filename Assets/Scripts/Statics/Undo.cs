﻿using System;
using System.Collections.Generic;

using MessageSystem;

namespace Utilities {

    public static class Undo {

        private readonly static Stack<Action<MessageClass>> stackUndoActions = new Stack<Action<MessageClass>>(64);
        private readonly static Stack<MessageClass> stackUndoMessages = new Stack<MessageClass>(64);

        public static bool HasHistory => stackUndoActions.Count > 0;

        public static Action onStackChange;

        public static void RecordAction(Action<MessageClass> action, MessageClass message) {
            stackUndoActions.Push(action);
            stackUndoMessages.Push(message);
            onStackChange?.Invoke();
        }

        public static bool ExecuteUndo() {
            if (stackUndoActions.Count == 0) {
                return false;
            }

            stackUndoActions.Pop().Invoke(stackUndoMessages.Pop());
            onStackChange?.Invoke();
            return true;
        }

        public static void Clear() {
            stackUndoActions.Clear();
            stackUndoMessages.Clear();
            onStackChange?.Invoke();
        }
    }
}