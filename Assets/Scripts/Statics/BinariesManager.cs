﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using UnityEngine;

namespace Utilities.Binaries {

    public static class BinariesManager {

        public static T Load<T>(BinaryFileTypes binaryFileType) where T : class {
            string filename = binaryFileType.ToString();
            string filepath = GetFilepath(filename);
            T data = null;

            if (File.Exists(filepath)) {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream fileStream = File.Open(filepath, FileMode.Open);

                data = binaryFormatter.Deserialize(fileStream) as T;
                fileStream.Close();
            }

            if (data != null) {
                Log.Debug($"File loaded: {filename}");
            }

            return data;
        }

        public static void Save<T>(T data, BinaryFileTypes binaryFileType) where T : class {
            string filename = binaryFileType.ToString();
            string filepath = GetFilepath(filename);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = File.Open(filepath, FileMode.OpenOrCreate);

            binaryFormatter.Serialize(fileStream, data);
            fileStream.Close();

            Log.Debug($"File saved: {filename}");
        }

        public static void Delete(BinaryFileTypes binaryFileType) {
            string filename = binaryFileType.ToString();
            string filepath = GetFilepath(filename);
            if (File.Exists(filepath)) {
                File.Delete(filepath);
            }
        }

        private static string GetFilepath(string filename) => Path.Combine(Application.persistentDataPath, $"{filename}.bin");
    }

    public enum BinaryFileTypes {
        YourFilename,
    }
}