﻿using UnityEngine;

namespace Utilities {

    // This is because using Camera.main do a FindObject and it is very expensive.
    public static class MainCamera {

        private static Camera instance;
        public static Camera Instance {
            get {
                if (instance == null) {
                    instance = Camera.main;
                }

                return instance;
            }
        }
    }
}