﻿public class PopUp : UIObject {

    // If the graphic designer like that the popups will have a animation or a special behaviour, here you can write the necessary code.
    public virtual void Show() {
        gameObject.SetActive(true);
    }

    public virtual void Hide() {
        gameObject.SetActive(false);
    }
}