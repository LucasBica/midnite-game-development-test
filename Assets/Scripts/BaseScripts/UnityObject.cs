﻿using UnityEngine;

public class UnityObject : MonoBehaviour {

    private Transform myTransform;
#pragma warning disable IDE1006 // Naming Styles
    public new Transform transform {
#pragma warning restore IDE1006 // Naming Styles
        get {
            if (myTransform == null) {
                myTransform = base.transform;
            }
            return myTransform;
        }
    }

    private GameObject myGameObject;
#pragma warning disable IDE1006 // Naming Styles
    public new GameObject gameObject {
#pragma warning restore IDE1006 // Naming Styles
        get {
            if (myGameObject == null) {
                myGameObject = base.gameObject;
            }
            return myGameObject;
        }
    }

    public void ForceAwake() {
        gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}