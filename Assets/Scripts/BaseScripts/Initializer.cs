﻿using UnityEngine;

namespace Namespace {

    public class Initializer : UIObject {

        [Header("References")]
        [SerializeField] private UnityObject[] unityObjects = default;

        private void Awake() {
            for (int i = 0; i < unityObjects.Length; i++) {
                unityObjects[i].ForceAwake();
            }
        }
    }
}