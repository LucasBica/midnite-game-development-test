﻿using UnityEngine;

public class UIObject : UnityObject {

    private RectTransform rectT;
    public RectTransform RectT {
        get {
            if (rectT == null) {
                rectT = GetComponent<RectTransform>();
            }
            return rectT;
        }
    }
}