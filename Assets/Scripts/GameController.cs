﻿using System.Collections.Generic;

using MessageSystem;

using UnityEngine;

using Utilities;

namespace Namespace {

    public class GameController : UnityObject {

        [Header("Settings")]
        [SerializeField] private LayerMask layerIngredients = default;
        [Space]
        [SerializeField] private int widthGrid = default;
        [SerializeField] private int heightGrid = default;
        [SerializeField] private int amountOfAdditionalPieces = default;

        [Header("Assets")]
        [SerializeField] private ObjectGrid prefabObjectGrid = default;

        [Header("References")]
        [SerializeField] private InputManager inputManager = default;

        private List<ObjectGrid>[,] grid;
        private bool levelCompleted;
        private int directionsLenght;

        private void Awake() {
            inputManager.OnSwipe += OnSwipe;
            directionsLenght = System.Enum.GetNames(typeof(Direction)).Length;

            if (widthGrid < 1) {
                widthGrid = 2;
            }

            if (heightGrid < 1) {
                heightGrid = 2;
            }

            if (amountOfAdditionalPieces < 0) {
                amountOfAdditionalPieces = 0;
            }

            int maxIngredients = widthGrid * heightGrid - 2;
            if (amountOfAdditionalPieces > maxIngredients) {
                amountOfAdditionalPieces = maxIngredients;
            }

            CreateLevel();
        }

        private void CreateLevel() {
            grid = new List<ObjectGrid>[widthGrid, heightGrid];
            for (int x = 0; x < widthGrid; x++) {
                for (int y = 0; y < heightGrid; y++) {
                    grid[x, y] = new List<ObjectGrid>(2);
                }
            }

            Vector2Int rootCoor = new Vector2Int(Random.Range(0, widthGrid), Random.Range(0, heightGrid));
            List<Vector2Int> availableCoors = new List<Vector2Int>(8);
            availableCoors.AddRange(GetNeighboringCells(rootCoor));

            ObjectGrid objectGridRoot = Instantiate(prefabObjectGrid, new Vector3(rootCoor.x, 0, rootCoor.y), Quaternion.identity);
            objectGridRoot.Initialize(rootCoor, 0);
            grid[rootCoor.x, rootCoor.y].Add(objectGridRoot);

            int amountToInstantiate = amountOfAdditionalPieces + 1;
            for (int i = 0; i < amountToInstantiate; i++) {
                Vector2Int randomCoor = availableCoors[Random.Range(0, availableCoors.Count - 1)];
                availableCoors.Remove(randomCoor);

                ObjectGrid objectGrid = Instantiate(prefabObjectGrid, new Vector3(randomCoor.x, 0, randomCoor.y), Quaternion.identity);
                objectGrid.Initialize(randomCoor, i);
                grid[randomCoor.x, randomCoor.y].Add(objectGrid);

                List<Vector2Int> emptyCells = GetEmptyCells(GetNeighboringCells(randomCoor));

                if (emptyCells.Count > 0) {
                    for (int j = 0; j < emptyCells.Count; j++) {
                        if (!availableCoors.Contains(emptyCells[j])) {
                            availableCoors.Add(emptyCells[j]);
                        }
                    }
                }   
            }
        }

        private void OnSwipe(Vector2 startPixelPosition, Vector2Int direction) {
            if (levelCompleted) {
                return;
            }

            Ray ray = MainCamera.Instance.ScreenPointToRay(startPixelPosition);
            if (!Physics.Raycast(ray, out RaycastHit hitInfo, int.MaxValue, layerIngredients)) {
                return;
            }

            ObjectGrid objectGridToMove = hitInfo.collider.GetComponent<ObjectGrid>();
            if (objectGridToMove == null) {
                return;
            }

            List<ObjectGrid> cellSelected = grid[objectGridToMove.Coor.x, objectGridToMove.Coor.y];

            if (cellSelected.Count == 0) {
                Log.Error($"[OnSwipe] ObjectGrid and coordinates do not match: {objectGridToMove.Coor}");
                return;
            }

            Vector2Int destinationCoordinates = objectGridToMove.Coor + direction;
            if (!IsValidCoor(destinationCoordinates)) {
                return;
            }

            List<ObjectGrid> cellDestination = grid[destinationCoordinates.x, destinationCoordinates.y];
            if (cellDestination.Count == 0) {
                return;
            }

            if (cellSelected[cellSelected.Count - 1].Value == 0 && cellDestination[cellDestination.Count - 1].Value == 0) {
                return; // Both objects are bread;
            }

            cellSelected[0].Move(direction, cellSelected, cellDestination);
            cellSelected.Reverse();
            cellDestination.AddRange(cellSelected);
            for (int i = 0; i < cellSelected.Count; i++) {
                cellSelected[i].Coor = destinationCoordinates;
            }
            cellSelected.Clear();

            if (IsThisLevelCompleted()) {
                levelCompleted = true;
                MessageManager.SendMessage(MessageTypes.ShowPopUpVictory);
            }
        }

        private bool IsThisLevelCompleted() {
            List<ObjectGrid> cell = null;
            int cellsFull = 0;

            for (int x = 0; x < widthGrid; x++) {
                for (int y = 0; y < heightGrid; y++) {
                    if (grid[x, y].Count > 0) {
                        if (cellsFull >= 1) {
                            return false;
                        }
                        cell = grid[x, y];
                        cellsFull++;
                    }
                }
            }

            if (cellsFull == 0) {
                Log.Error("[IsThisLevelCompleted] The grid is empty");
                return false;
            }

            if (cell.Count == 0) {
                Log.Error("[IsThisLevelCompleted] cell.Count is zero");
                return false;
            }

            if (cell[0].Value != 0 || cell[cell.Count - 1].Value != 0) {
                return false;
            }

            return true;
        }

        private List<Vector2Int> GetEmptyCells(List<Vector2Int> coors) {
            for (int i = coors.Count - 1; i >= 0; i--) {
                List<ObjectGrid> cell = grid[coors[i].x, coors[i].y];
                if (cell.Count != 0) {
                    coors.RemoveAt(i);
                }
            }

            return coors;
        }

        private List<Vector2Int> GetNeighboringCells(Vector2Int coor) {
            if (!IsValidCoor(coor)) {
                Log.Error($"[GetNeighboringCells] Invalid coor: {coor}");
                return null;
            }

            List<Vector2Int> validCoors = new List<Vector2Int>(4);

            for (int i = 0; i < directionsLenght; i++) {
                Direction direction = (Direction)i;
                Vector2Int directionValue;
                Vector2Int validCoor;

                switch (direction) {
                    case Direction.Up:
                        directionValue = Vector2Int.up;
                        break;
                    case Direction.Right:
                        directionValue = Vector2Int.right;
                        break;
                    case Direction.Down:
                        directionValue = Vector2Int.down;
                        break;
                    case Direction.Left:
                        directionValue = Vector2Int.left;
                        break;
                    default:
                        Log.EnumNotImplemented(direction.ToString());
                        return null;
                }

                validCoor = coor + directionValue;

                if (IsValidCoor(validCoor)) {
                    validCoors.Add(validCoor);
                }
            }

            return validCoors;
        }

        private bool IsValidCoor(Vector2Int coor) => coor.x >= 0 && coor.x < grid.GetLength(0) && coor.y >= 0 && coor.y < grid.GetLength(1);
    }
}