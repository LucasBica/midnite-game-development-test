﻿using System;
using System.Collections.Generic;
using System.Linq;

using Utilities;

namespace MessageSystem {

    public static class MessageManager {

        private static readonly Dictionary<MessageTypes, List<Action<MessageClass>>> listeners = new Dictionary<MessageTypes, List<Action<MessageClass>>>();
        private static readonly Dictionary<MessageTypes, List<MessageClass>> pendingMessagesToSend = new Dictionary<MessageTypes, List<MessageClass>>();
        private static bool updateActive;

        public static void AttachListener(MessageTypes type, Action<MessageClass> handler) {
            if (!listeners.ContainsKey(type)) {
                listeners.Add(type, new List<Action<MessageClass>>());
            }

            List<Action<MessageClass>> listenerList = listeners[type];

            if (listenerList.Contains(handler)) {
                Log.Error($"[MessageSystem] Attached duplicate listener: {type}");
            } else {
                listenerList.Add(handler);
            }
        }

        public static void DetachListener(MessageTypes type, Action<MessageClass> handler) {
            if (!listeners.ContainsKey(type)) {
                Log.Error($"[MessageManager] Detached non-existant listener: {type}");
            } else {
                listeners[type].Remove(handler);
                if (listeners[type].Count == 0) {
                    listeners.Remove(type);
                }
            }
        }

        public static bool SendMessage(MessageTypes type, MessageClass messageClass = null, bool isAsyncCall = false) {
            if (!listeners.ContainsKey(type)) {
                return false;
            }

            if (isAsyncCall) {
                if (!updateActive) {
                    updateActive = true;
                    Singleton.Instance.onUpdate += OnUpdate;
                }

                if (pendingMessagesToSend.TryGetValue(type, out List<MessageClass> messages)) {
                    messages.Add(messageClass);
                } else {
                    pendingMessagesToSend.Add(type, new List<MessageClass> {
                        messageClass
                    });
                }
            } else {
                SendMessage(type, messageClass);
            }

            return true;
        }

        private static void SendMessage(MessageTypes type, MessageClass messageClass = null) {
            List<Action<MessageClass>> handler = listeners[type];
            for (int i = handler.Count - 1; i >= 0; i--) { // The order of the call is not important, the important thing is that it does not explode if one is detached during the loop.
                handler[i](messageClass);
            }
        }

        private static void OnUpdate() {
            Singleton.Instance.onUpdate -= OnUpdate;
            updateActive = false;

            KeyValuePair<MessageTypes, List<MessageClass>>[] messageToSend = pendingMessagesToSend.ToArray();

            for (int i = 0; i < messageToSend.Length; i++) {
                for (int j = 0; j < messageToSend[i].Value.Count; j++) {
                    SendMessage(messageToSend[i].Key, messageToSend[i].Value[j]);
                }
            }

            pendingMessagesToSend.Clear();
        }
    }
}