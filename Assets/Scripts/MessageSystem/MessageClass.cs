﻿using System;

namespace MessageSystem {

    [Serializable]
    public abstract class MessageClass { }

    // If you need to send parameters along with the message, you must create a class that contains all the required parameters and inherits from MessageClass.

    public class MessageUndo : MessageClass {
        // Dates for do a undo.
    }

    public class ValueMessage<T> : MessageClass {
        public T value;
    }
}