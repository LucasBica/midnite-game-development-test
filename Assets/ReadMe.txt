READ ME:

I developed only Task A for time reasons, but after seeing the required points in Task B, I realized that I had already done some of them or I only had to expose some fields.

Add in some scripts that I always use, all previously created by me. You can see how I would implement the Undo button, which I would use to restart the level without changing the initial positions of the ingredients like in the original game.

On the subject of serialization, I could do it in binary or in ScriptableObject according to how you want, I would add an editor in Unity to work more comfortably, if not, another option would be to establish certain values ​​for the generation of levels and that these change as that the player advances, adding greater difficulty.

In the folder "Assets / Scripts / MessageSystem" you can see some scripts that I normally use for the communication of the scripts without having to fill the entire scene with references, it also makes it easier for me that the objects that are created during the level, can do attach and detach according to what you need, but its main use is to send calls in async methods and that they are listened to by multiple scripts at the same time.